import {
	DOCTOR_LIST_SUCCESS,
  DOCTOR_LIST_SPINNER,
  SET_RENDER_DOCTOR_LIST,
  REMOVE_RENDER_DOCTOR_LIST,
} from '../actions/types';

const INITIAL_STATE = {
  doctors: null,
  loading: false,
  render: false,
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case DOCTOR_LIST_SPINNER:
      return { ...state, loading: true }
    case DOCTOR_LIST_SUCCESS:
      return { ...state, doctors: action.payload, loading: false }
    case SET_RENDER_DOCTOR_LIST:
      return { ...state, render: true }
    case REMOVE_RENDER_DOCTOR_LIST:
      return { ...state, render: false }
    default:  
      return state;
  }
}