import {
  DOCTOR_INPUT_CHANGED,
  DOCTOR_NAME_CHANGED,
  DOCTOR_SPECIALITY_CHANGED,
  DOCTOR_QUALIFICATION_CHANGED,
  DOCTOR_PHONE_CHANGED,
  DOCTOR_CREATE_SPINNER,
  DOCTOR_REMOVE_SPINNER,
  CHANGE_FORM_MAP_MARKER,
  DOCTOR_INITIAL_STATE
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  speciality: '',
  qualification: '',
  phone: '',
  city: '',
  loading: false,
  region: {
    latitude: 31.582045,
    longitude: 74.329376,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
  },
  coordinates: {
    latitude: 31.582045,
    longitude: 74.329376,
  },
  update: false,
  // regionLatitude: null,
  // regionLongitude: null,
  // markerLatitude: null,
  // markerLongitude: null,
  // currentLongitude: null,
  // currentLatitude: null,
  // latitudeDelta: 0.015,
  // longitudeDelta: 0.0121,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DOCTOR_NAME_CHANGED:
      return { ...state, name: action.payload };
    case DOCTOR_INPUT_CHANGED:
      return { ...state, [action.payload.prop]: action.payload.value };
    case DOCTOR_SPECIALITY_CHANGED:
      return { ...state, speciality: action.payload };
    case DOCTOR_QUALIFICATION_CHANGED:
      return { ...state, qualification: action.payload };
    case DOCTOR_PHONE_CHANGED:
      return { ...state, phone: action.payload };
    case DOCTOR_CREATE_SPINNER:
      return { ...state, loading: true };
    case DOCTOR_REMOVE_SPINNER:
      return { ...state, loading: false };
    case CHANGE_FORM_MAP_MARKER:
      return { ...state, region: action.payload.region, coordinates: action.payload.coordinates };
    case DOCTOR_INITIAL_STATE:
      return { ...state, ...INITIAL_STATE };
    default:
      return state;
  }
}