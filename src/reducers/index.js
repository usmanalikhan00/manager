import { combineReducers } from 'redux'; 
import AuthReducer from './AuthReducer';
import EmployeeFormReducer from './EmployeeFormReducer';
import EmployeesListReducer from './EmployeesListReducer';
import DoctorListReducer from './DoctorListReducer';
import DoctorFormReducer from './DoctorFormReducer';
import ViewDoctorReducer from './ViewDoctorReducer';
import MedicineFormReducer from './MedicineFormReducer';
import MedicineListReducer from './MedicineListReducer';
import RoutesReducer from './RoutesReducer';
import AssignDoctorMedicineReducer from './AssignDoctorMedicineReducer';
import AssignDoctorsReducer from './AssignDoctorsReducer';
import EmployeeDoctorsReducer from './EmployeeDoctorsReducer';

export default combineReducers({
	auth: AuthReducer,
	employees: EmployeesListReducer,
	employeeForm: EmployeeFormReducer,
	doctors: DoctorListReducer,
	assignDoctors: AssignDoctorsReducer,
	doctorForm: DoctorFormReducer,
	selectedDoctor: ViewDoctorReducer,
	medicineForm: MedicineFormReducer,
	medicines: MedicineListReducer,
	activeRoute: RoutesReducer,
	assignMedicine: AssignDoctorMedicineReducer,
	employeeDoctors: EmployeeDoctorsReducer,
});