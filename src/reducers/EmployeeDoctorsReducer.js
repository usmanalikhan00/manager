import {
  GET_EMPLOYEE_DOCTORS,
  EMPLOYEE_DOCTORS_SPINNER_CREATE,
  EMPLOYEE_DOCTORS_SPINNER_REMOVE,
} from "../actions/types";

const INITIAL_STATE = {
  userId: null,
  loading: false,
  doctors: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_EMPLOYEE_DOCTORS:
      return { ...state, userId: action.payload.uid, doctors: action.payload.doctors }
    case EMPLOYEE_DOCTORS_SPINNER_CREATE:
      return { ...state, loading: true }
    case EMPLOYEE_DOCTORS_SPINNER_REMOVE:
      return { ...state, loading: false }
    default:
      return state;
  }
}