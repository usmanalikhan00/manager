import {
  VIEW_DOCTOR,
  VIEW_DOCTOR_SPINNER,
  CLEAR_SELECTED_DOCTOR,
  CLEAR_DOCTOR_SPINNER,
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  qualification: '',
  speciality: '',
  phone: '',
  city: '',
  coordinates: '',
  loading: false,
  medicinesUid: [],
  medicinesData: [],
  uid: '',
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case VIEW_DOCTOR:
      return { 
        ...state, 
        name: action.payload.name, 
        qualification: action.payload.qualification, 
        speciality: action.payload.speciality, 
        phone: action.payload.phone, 
        city: action.payload.city, 
        coordinates: action.payload.coordinates, 
        medicinesUid: action.payload.medicinesUid,
        medicinesData: action.payload.medicinesData,
        uid: action.payload.uid,
      };
    case VIEW_DOCTOR_SPINNER:
      return { 
        ...state, 
        loading: true, 
      };
    case CLEAR_DOCTOR_SPINNER:
      return { 
        ...state, 
        loading: false, 
      };
    case CLEAR_SELECTED_DOCTOR:
      return { 
        ...state, 
        ...INITIAL_STATE, 
      };

    default:
      return state;
  }
}