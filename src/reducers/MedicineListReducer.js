import {
  MEDICINE_CREATE_LIST_SPINNER,
  MEDICINE_LIST_SUCCESS,
} from "../actions/types";

const INITIAL_STATE = {
  medicines: null,
  loading: false
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MEDICINE_CREATE_LIST_SPINNER:
      return { ...state, loading: true };
    case MEDICINE_LIST_SUCCESS:
      return { ...state, medicines: action.payload, loading: false };
    default:
      return state;
  }
}