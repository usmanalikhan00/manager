import {
  SET_SELECTED_MEDICINE_UID,
  SET_SELECTED_MEDICINE_DATA,
  REMOVE_SELECTED_MEDICINE_UID,
  REMOVE_SELECTED_MEDICINE_DATA,
  SET_MEDICINES,
} from "../actions/types";

const INITIAL_STATE = {
  medicinesUid: [],
  medicinesData: [],
}


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SELECTED_MEDICINE_DATA:
      let medicinesCopy = state.medicinesData.slice();
      medicinesCopy.push(action.payload);
      return { ...state, medicinesData: medicinesCopy };
    case SET_SELECTED_MEDICINE_UID:
      return { ...state, medicinesUid: action.payload };
    case REMOVE_SELECTED_MEDICINE_DATA:
      return { ...state, medicinesData: action.payload };
    case REMOVE_SELECTED_MEDICINE_UID:
      return { ...state, medicinesUid: action.payload };
    case SET_MEDICINES:
      return {
        ...state,
        medicinesUid: action.payload.medicinesUid,
        medicinesData: action.payload.medicinesData
      };
    default:
      return state;
  }
}