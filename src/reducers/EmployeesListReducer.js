import {
	EMPLOYEES_LIST_SUCCESS,
	EMPLOYEES_LIST_SPINNER
} from '../actions/types';

const INITIAL_STATE = {
	employees: null,
	loading: false
};

export default (state = INITIAL_STATE, action) => {
	switch(action.type){
		case EMPLOYEES_LIST_SUCCESS:
			return {...state, employees: action.payload, loading: false};
		case EMPLOYEES_LIST_SPINNER:
			return {...state, ...INITIAL_STATE, loading: true};
		default:
			return state;
	}
}