import {
  REMOVE_SELECTED_DOCTOR_DATA,
  SET_SELECTED_DOCTOR_DATA,
  SET_SELECTED_DOCTOR_UID,
  SET_USER_FORM_DOCTORS,
  RESET_USER_FORM_DOCTORS,
  ADD_USER_FORM_DOCTOR_SPINNER,
  REMOVE_USER_FORM_DOCTOR_SPINNER,
} from "../actions/types";

const INITIAL_STATE = {
  doctorsUid: [],
  doctorsData: [],
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SELECTED_DOCTOR_DATA:
        let doctorsCopy = state.doctorsData.slice();
        doctorsCopy.push(action.payload.doctor);
      return { ...state, doctorsData: doctorsCopy }
    case SET_SELECTED_DOCTOR_UID:
      return { ...state, doctorsUid: action.payload }
    case REMOVE_SELECTED_DOCTOR_DATA:
      return { ...state, doctorsUid: action.payload.doctorsUid, doctorsData: action.payload.doctorsData }
    case SET_USER_FORM_DOCTORS:
      return { ...state, doctorsUid: action.payload.doctorsUid.slice(), doctorsData: action.payload.doctors.slice() }
    case RESET_USER_FORM_DOCTORS:
      return { ...state, doctorsUid: action.payload.doctorsUid.slice(), doctorsData: action.payload.doctors.slice() }
    case ADD_USER_FORM_DOCTOR_SPINNER:
      return { ...state, loading: true }
    case REMOVE_USER_FORM_DOCTOR_SPINNER:
      return { ...state, loading: false }
    default:
      return state;
  }
}