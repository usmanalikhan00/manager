import {
  MEDICINE_CREATE_SPINNER,
  MEDICINE_REMOVE_SPINNER,
  MEDICINE_FORM_RESET,
  MEDICINE_INPUT_CHANGED,
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  salt: '',
  dosage: '',
  unit: '',
  type: '',
  price: '',
  purpose: '',
  loading: false,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MEDICINE_INPUT_CHANGED:
      return { ...state, [action.payload.prop]: action.payload.value };
    case MEDICINE_CREATE_SPINNER:
      return { ...state, loading: true };
    case MEDICINE_REMOVE_SPINNER:
      return { ...state, loading: false };
    case MEDICINE_FORM_RESET:
      return { ...state, ...INITIAL_STATE };
    default:
      return state;
  }
}