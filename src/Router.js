import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { Scene, Router, Actions, Modal, Stack, Drawer } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';
import DoctorList from './components/DoctorList';
import DoctorCreate from './components/DoctorCreate';
import DrawerContent from './components/DrawerContent';
import DoctorView from './components/DoctorView';
import DoctorEdit from './components/DoctorEdit';
import MedicineCreate from './components/MedicineCreate';
import MedicineList from './components/MedicineList';
import MedicineEdit from './components/MedicineEdit';
import AssignDoctorMedicine from './components/AssignDoctorMedicine';
import AssignedDoctorsModal from './components/AssignedDoctorsModal';
import AssignDoctors from './components/AssignDoctors';
import NavBar from './components/NavBar';
import { connect } from 'react-redux';
import { checkLogged, doctorList } from './actions';
import { LoadingScreen } from './components/common';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';

import {
	EMPLOYEE_ADD_ACTION,
	DOCTOR_ADD_ACTION,
	MEDICINE_ADD_ACTION,
} from './Constants';

class RouterComponent extends React.Component {

	componentDidMount() {
		this.props.checkLogged();
	}

	renderAddButton = (onPress) => {
		return (
			<TouchableWithoutFeedback onPress={onPress}>
				<Ionicons style={{ paddingRight: 10 }} name="ios-add-circle" size={30} color="#4F8EF7" />
			</TouchableWithoutFeedback>
		);
	}

	renderMenuButton = () => {
		return (
			<FontAwesome name="bars" size={28} color="#4F8EF7" />
		);
	}

	renderBackButton = () => {
		return (
			<TouchableWithoutFeedback onPress={() => Actions.pop()}>
				{/* <Ionicons style={{ paddingLeft: 10 }} name="ios-arrow-back" size={30} color="#4F8EF7" /> */}
				<Icon style={{ paddingLeft: 10 }} size={30} color="#4F8EF7" name="arrowleft" />
			</TouchableWithoutFeedback>
		);
	}

	renderRightMenuButton = () => {
		return (
			<TouchableWithoutFeedback onPress={() => Actions.drawerOpen()}>
				<FontAwesome style={{ paddingRight: 10 }} name="bars" size={28} color="#4F8EF7" />
			</TouchableWithoutFeedback>
		);
	}



	renderScenes() {
		if (this.props.user) {
			return (
				Actions.create(
					<Modal>
						<Stack
							key="drawer"
							hideNavBar
							drawerIcon={this.renderMenuButton}
							drawer
							drawerWidth={250}
							contentComponent={DrawerContent}
						>
							<Scene
								key="root"
								initial
							>
								<Scene
									key="employeeList"
									component={EmployeeList}
									title="Employees"
									renderRightButton={() => this.renderAddButton(EMPLOYEE_ADD_ACTION)}
								/>
								<Scene
									key="employeeCreate"
									title="Create Employee"
									component={EmployeeCreate}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="employeeEdit"
									component={EmployeeEdit}
									title="Edit Employee"
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="assignDoctor"
									component={AssignDoctors}
									title="Assign Doctors"
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
								/>
								<Scene
									key="doctorList"
									component={DoctorList}
									title="Doctors"
									renderRightButton={() => this.renderAddButton(DOCTOR_ADD_ACTION)}
								/>
								<Scene
									key="doctorCreate"
									title="Create Doctor"
									component={DoctorCreate}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="doctorView"
									title="Doctor Information"
									component={DoctorView}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="doctorEdit"
									title="Doctor Edit"
									component={DoctorEdit}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="assignDoctorMedicine"
									title="Assign Medicines"
									component={AssignDoctorMedicine}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									// renderLeftButton={() => this.renderBackButton()}
								/>
								<Scene
									key="medicineList"
									component={MedicineList}
									title="Medicines"
									renderRightButton={() => this.renderAddButton(MEDICINE_ADD_ACTION)}
								/>
								<Scene
									key="medicineEdit"
									component={MedicineEdit}
									title="Edit Medicine"
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
								/>
								<Scene
									key="medicineCreate"
									title="Create Medicine"
									component={MedicineCreate}
									drawerLockMode={'locked-closed'}
									hideDrawerButton={true}
									onRight={() => Actions.assignedDoctorsModal()}
									rightTitle="assigned"
								/>
							</Scene>
						</Stack>
						<Scene
							key="assignedDoctorsModal"
							component={AssignedDoctorsModal}
						/>
					</Modal>
				)
			)
		}
		else {
			return (
				Actions.create(
					<Stack
						key="root"
						hideNavBar
						drawer
						drawerWidth={250}
						contentComponent={DrawerContent}
					>
						<Scene
							key="auth"
							drawerLockMode={'locked-closed'}
							hideDrawerButton={true}
							initial
						>
							<Scene
								key="login"
								component={LoginForm}
								title="Please Login"
							/>
						</Scene>
					</Stack>
				)
			)
		}
	}


	render() {
		return (
			<Router scenes={this.renderScenes()} />
		);
	}
}

const mapStateToProps = (state) => {
	return {
		user: state.auth.user
	};
};


export default connect(mapStateToProps, { checkLogged, doctorList })(RouterComponent);