import React, { Component } from 'react';
import { YellowBox } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import Router from './Router';
import { composeWithDevTools } from 'redux-devtools-extension';
// import { composeWithDevTools } from 'remote-redux-devtools';
const middlewares = [ReduxThunk]; 
class App extends Component {

	componentDidMount() {
		YellowBox.ignoreWarnings([
			'Warning: componentWillReceiveProps is deprecated and will be removed in the next major version. Use static getDerivedStateFromProps instead.',
			'Warning: componentWillMount is deprecated and will be removed in the next major version. Use componentDidMount instead. As a temporary workaround, you can rename to UNSAFE_componentWillMount.',
			'Setting a timer',
		]);
		// YellowBox.ignoreWarnings([]);
		// YellowBox.ignoreWarnings([]);
		const firebaseConfig = {
			apiKey: "AIzaSyC7GT_XU9xJog4J7KcC3ygiYvSNISYFqIA",
			authDomain: "managers-9499d.firebaseapp.com",
			databaseURL: "https://managers-9499d.firebaseio.com",
			projectId: "managers-9499d",
			storageBucket: "managers-9499d.appspot.com",
			messagingSenderId: "271861781029",
			appId: "1:271861781029:web:3f2e364f1098e196"
		};
		firebase.initializeApp(firebaseConfig);
	}

	render() {

		// const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
		// const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
			const composeEnhancers = composeWithDevTools({ realtime: true });
		const store = createStore(
			reducers,
			{},
			composeEnhancers(applyMiddleware(...middlewares))
		);
		// const store = createStore(
		// 	reducers,
		// 	{},
		// 	composeWithDevTools(applyMiddleware(ReduxThunk)),
		// 	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
		// );

		return (
			<Provider store={store}>
				<Router />
			</Provider>
		);
	}
}

export default App;