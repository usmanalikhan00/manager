import React, {Component} from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';
import { CardSection, Input } from './common';
import { employeeUpdate } from '../actions';

class EmployeeForm extends Component {
	render() {
		return(		
			<View>
				<CardSection>
					<Input
						onChangeText={
							value => this.props.employeeUpdate({ prop: 'name', value })
						}
						value={this.props.name}
						label="Name"
						placeholder="Jane"
					/>
				</CardSection>
				<CardSection>
					<Input
						onChangeText={
							value => this.props.employeeUpdate({ prop: 'phone', value })
						}
						value={this.props.phone}
						label="Phone"
						placeholder="333-555-444"
					/>
				</CardSection>
				<CardSection>
					<Text style={styles.pickerLabelStyle}>Shift Day</Text>
					<Picker
						style={{ flex: 1 }}
						selectedValue={this.props.shift}
						onValueChange={value => this.props.employeeUpdate({ prop: 'shift', value })}
					>
						<Picker.Item label="Monday" value="Monday" />
						<Picker.Item label="Tuesday" value="Tuesday" />
						<Picker.Item label="Wednesday" value="Wednesday" />
						<Picker.Item label="Thursday" value="Thursday" />
						<Picker.Item label="Friday" value="Friday" />
						<Picker.Item label="Saturday" value="Saturday" />
						<Picker.Item label="Sunday" value="Sunday" />
					</Picker>
				</CardSection>
			</View>
		);
	}
}

const styles = {
	pickerLabelStyle: {
		fontSize: 16,
		paddingLeft: 20
	}
}

const mapStateToProps = state => {
	const { name, phone, shift, coordinates, } = state.employeeForm;
	return { name, phone, shift, coordinates, };
};

export default connect(mapStateToProps, { employeeUpdate })(EmployeeForm);

