import React, { Component } from 'react';
import { Text } from 'react-native';
import { Card, CardSection, Button, Spinner } from './common';
import DoctorForm from './DoctorForm';
import { connect } from 'react-redux';
import { doctorCreate, doctorReset } from '../actions';
import FormsMapWidget from './FormsMapWidget';


class DoctorCreate extends Component {

	state = {
		coordinates: {
			latitude: 37.77839052733534,
			longitude: -122.4467659369111,
		},
	}

	onButtonPress() {
		const { name, speciality, qualification, phone, city, } = this.props;
		const { coordinates, } = this.state;
		this.props.doctorCreate({ name, speciality, qualification, phone, city, coordinates, })
	}

	renderButton() {
		if (this.props.loading) {
			return (
				<Spinner size="large" />
			);
		}
		return (
			<Button onPress={this.onButtonPress.bind(this)}>
				Create
			</Button>
		);
	}

	componentWillUnmount() {
		// this.props.doctorReset();
	}
	
	componentDidMount() {
		this.props.doctorReset();
	}


	setCoordinates = (coordinates) => {
		this.setState({
			coordinates: {
				...this.state.coordinates,
				latitude: coordinates.latitude,
				longitude: coordinates.longitude,
			}
		});
	}

	render() {
		return (
			<Card>
				<DoctorForm {...this.props} />
				<CardSection>
					<Text style={styles.labelStyle}>Select Location</Text>
				</CardSection>
				<CardSection>
					<FormsMapWidget getCoordinates={this.setCoordinates} />
				</CardSection>
				<CardSection>
					{this.renderButton()}
				</CardSection>
			</Card>
		);
	}
}

const mapStateToProps = (state) => {
	const {
		name,
		speciality,
		qualification,
		phone,
		city,
		loading,
	} = state.doctorForm;
	return {
		name,
		speciality,
		qualification,
		phone,
		city,
		loading,
	};
}

const styles = {
	labelStyle: {
		fontSize: 16,
		paddingLeft: 20,
		paddingTop: 10,
		paddingBottom: 10,
		flex: 1
	},
}


export default connect(mapStateToProps, { doctorCreate, doctorReset })(DoctorCreate);