import React, { Component } from 'react';
import { Text, View } from 'react-native';
import MedicineForm from './MedicineForm';
import { Card, CardSection, Button, Spinner } from './common';
import { connect } from 'react-redux';
import { medicineCreate, medicineFormReset } from "../actions";

class MedicineCreate extends Component {

  employeeCreate() {
    const { name, salt, dosage, unit, type, price, purpose } = this.props;
    this.props.medicineCreate(name, salt, dosage, unit, type, price, purpose);
  }

  renderButton() {
    if (this.props.loading){
      return (
        <Spinner />
      );
    }
    return (
      <Button onPress={() => this.employeeCreate()}>
        Save
      </Button>
    )
  }

  componentWillMount() {
    this.props.medicineFormReset();
  }

  render () {
    return (
      <Card>
        <MedicineForm {...this.props} />
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }

}

const mapStateToProps = (state) => {
  const {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
    loading,
  } = state.medicineForm;

  return {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
    loading,
  };
}

export default connect(mapStateToProps, { medicineCreate, medicineFormReset })(MedicineCreate);

