import React from 'react';
import { Text, View, Picker } from 'react-native';
import { connect } from 'react-redux';
import { doctorList, selectDoctor } from "../actions";
import { CardSection, Card } from './common';
import { Button } from "./common";

class DoctorPicker extends React.Component {

  state = {
    value: null
  }
  
  componentDidMount() {
    this.props.doctorList();
  }
  
  render() {
    // console.log("PROPS FROM DOCTOR PICKER:--", this.props)
    return (
      <Card>
        <CardSection>
          <Text style={styles.pickerLabelStyle}>Pick Doctor</Text>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.state.value}
            onValueChange={value => this.setState({ value })}
          >
            {this.props.doctors && this.props.doctors.map((doctor, i) => {
              return (
                <Picker.Item label={doctor.name} value={doctor.uid} key={i} />
              );
            })}
          </Picker>
        </CardSection>
        <CardSection>
          <Button
          onPress={() =>
            this.props.selectDoctor(this.props.doctorsUid, this.state.value || this.props.doctors[0].uid)
          }
          >
            Select Doctor
          </Button>
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  pickerLabelStyle: {
    fontSize: 18,
    alignSelf: 'center',
    paddingLeft: 20
  }
}

const mapStateToProps = (state) => {
  const { doctors } = state.doctors;
  const { doctorsUid } = state.assignDoctors;
  return {
    doctors,
    doctorsUid
  };
}

export default connect(mapStateToProps, { doctorList, selectDoctor })(DoctorPicker);