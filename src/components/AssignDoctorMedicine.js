import React from 'react';
// import { View } from 'react-native';
import { connect } from 'react-redux';
import { updateMedicine, resetMedicines } from "../actions";
import MedicinePicker from "./MedicinePicker";
import MedicinePickerView from "./MedicinePickerView";
import { Card, Button, CardSection, Spinner } from './common';

class AssignDcotorMedicine extends React.Component {

  updateMedicine() {
    this.props.updateMedicine(this.props.uid, this.props.medicinesUid, this.props.medicinesData);
  }

  componentWillUnmount() {
    this.props.resetMedicines(this.props.doctorMedicinesUid, this.props.doctorMedicinesData)
  }

  renderButton() {
    if (this.props.loading) {
      return (
        <Spinner />
      )
    }
    return (
      <CardSection>
        <Button onPress={() => this.updateMedicine()}>
          Save
          </Button>
      </CardSection>
    );

  }

  render() {
    // console.log("PROPS FROM ASSIGN DOCTOR MEDICINE:---\n", this.props);
    return (
      <Card>
        <MedicinePicker />
        <MedicinePickerView />
        {this.renderButton()}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    uid: state.selectedDoctor.uid,
    doctorMedicinesUid: state.selectedDoctor.medicinesUid,
    doctorMedicinesData: state.selectedDoctor.medicinesData,
    loading: state.selectedDoctor.loading,
    medicinesUid: state.assignMedicine.medicinesUid,
    medicinesData: state.assignMedicine.medicinesData,
  }
}


export default connect(mapStateToProps, { updateMedicine, resetMedicines })(AssignDcotorMedicine);