import React, { Component } from 'react';
// import { connect } from 'react-redux';
import { Text, 
				 TouchableWithoutFeedback, 
				 View, } from 'react-native';
import { CardSection } from './common';
import { Actions } from 'react-native-router-flux';

class ListItem extends Component {

	onPress() {
		// Actions.employeeEdit({ employee: this.props.employee });
	}

	render() {
		const { name } = this.props.doctor;
		const { textStyle } = styles;
		
		return (
			<TouchableWithoutFeedback onPress={this.onPress.bind(this)}>
				<View>
					<CardSection>
						<Text 
							style={textStyle}
						>
							{name}
						</Text>
					</CardSection>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 18,
		padding: 10
	}
};

export default ListItem;