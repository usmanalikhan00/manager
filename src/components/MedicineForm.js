import React from 'react';
import { Text, View } from 'react-native';
import { Input, CardSection } from './common';
import { connect } from 'react-redux';
import { medicineInputChanged } from "../actions";

class MedicineForm extends React.Component {
  render() {
    return (
      <View>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'name', value })
            }
            value={this.props.name}
            label="Name"
            placeholder="Ciproxin"
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'dosage', value })
            }
            value={this.props.dosage}
            label="Dosage"
            placeholder="100"
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'unit', value })
            }
            value={this.props.unit}
            label="Unit"
            placeholder="e.g. mg, cc"
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'type', value })
            }
            value={this.props.type}
            label="Type"
            placeholder="Tab, Infusion etc."
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'salt', value })
            }
            value={this.props.salt}
            label="Salt"
            placeholder="Tab, Infusion etc."
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'price', value })
            }
            value={this.props.price}
            label="Price"
            placeholder="12"
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={
              value => this.props.medicineInputChanged({ prop: 'purpose', value })
            }
            value={this.props.purpose}
            label="Purpose"
            placeholder="Relaxent, Upper RTI's etc."
          />
        </CardSection>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
  } = state.medicineForm;

  return {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
  };
}


export default connect(
  mapStateToProps,
  {
    medicineInputChanged,
  }
)(MedicineForm);

