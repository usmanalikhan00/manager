import React from 'react';
import { Text, View } from "react-native";
import { connect } from "react-redux";
import { Card, CardSection, Chip } from './common';
import Icon from 'react-native-vector-icons/FontAwesome';
import { removeDoctor } from "../actions";

class DoctorPickerView extends React.Component {

  removeDoctor(doctor, i) {
    this.props.removeDoctor(this.props.doctorsUid, this.props.doctorsData, i);
  }

  renderContent() {
    if (!this.props.doctorsData.length) {
      return (
        <CardSection>
          <Text style={{ textAlign: 'center', flex: 1 }}>No Data</Text>
        </CardSection>
      );
    }
    return (
      <View>
        <CardSection style={{
          flexDirection: 'row',
          flexWrap: 'wrap'
        }}>
          {this.props.doctorsData && this.props.doctorsData.map((doctor, i) => {
            return (
              <Chip key={i} text={doctor.name} view={true} onPress={() => this.removeDoctor(doctor, i)} />
            );
          })}
        </CardSection>
      </View>
    );
  }

  render() {
    return (
      this.renderContent()
    );
  }
}

const mapStateToProps = state => {
  const { doctorsUid, doctorsData } = state.assignDoctors;
  return {
    doctorsUid,
    doctorsData,
  }
}

export default connect(mapStateToProps, { removeDoctor })(DoctorPickerView);