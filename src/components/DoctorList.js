import React, { Component } from 'react';
import { View, Text, Dimensions, FlatList, TouchableOpacity, Platform } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Animated, Marker, Callout } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { connect } from 'react-redux';
import { doctorList, viewDoctor, setActiveRoute, removeRenderDoctorList } from '../actions';
import * as _ from 'lodash';
import ListItem from './DoctorListItem';
import { Spinner, CardSection } from './common';
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('window');
const DEFAULT_PADDING = { top: 10, right: 10, bottom: 10, left: 10 };

const newRegion = {
  latitude: 31.582045,
  longitude: 74.329376,
  latitudeDelta: 0.015,
  longitudeDelta: 0.0121,
}

class DoctorList extends Component {

  state = {
    region: {
      latitude: 31.582045,
      longitude: 74.329376,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    },
    coordinates: {
      latitude: 31.582045,
      longitude: 74.329376,
    },
    location: null,
    selectedMarker: null,
  }

  mapRef = null
  markerRef = null

  // Geolocation.getCurrentPosition(
  //   position => {
  //     const location = position;
  //     this.setState({ location }, () => {
  //       this.setState({
  //         region: {
  //           ...this.state.region,
  //           latitude: this.state.location.coords.latitude,
  //           longitude: this.state.location.coords.longitude,
  //         },
  //         coordinates: {
  //           ...this.state.coordinates,
  //           latitude: this.state.location.coords.latitude,
  //           longitude: this.state.location.coords.longitude,
  //         }
  //       }, () => {
  //       });
  //     });
  //   },
  //   error => Alert.alert(error.message),
  //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, showLocationDialog: true }
  // );
  // console.log("COM");

  componentDidMount = () => {
    this.props.doctorList();
    this.props.setActiveRoute('Doctors');
  }

  renderContent() {
    if (this.props.loading) {
      return (
        <CardSection>
          <Spinner size="large" />
        </CardSection>
      );
    }
    return (
      <FlatList
        data={this.props.doctors}
        renderItem={({ item }) => <ListItem doctor={item}></ListItem>}
        keyExtractor={doctor => doctor.uid}
      />
    );
  }


  renderMapButtons() {
    if (this.props.loading) {
      return (
        <CardSection style={styles.container}>
          <Spinner size="large" />
        </CardSection>
      );
    }
    return (
      <View style={styles.btnContainer}>
        {/* <TouchableOpacity
          onPress={() => this.fitAllMarkers()}
          style={[styles.bubble, styles.button]}
        >
          <Text>Fit All Markers</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => this.fitPadding()}
          style={[styles.bubble, styles.button]}
        >
          <Text>Fit Markers</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.goToDefaultRegion()}
          style={[styles.bubble, styles.button]}
        >
          <Text>Set Region</Text>
        </TouchableOpacity>
      </View>
    );
  }



  fitAllMarkers() {
    this.mapRef.fitToCoordinates(this.props.markers, {
      edgePadding: DEFAULT_PADDING,
      animated: true,
    });
  }

  fitPadding() {
    this.mapRef.fitToCoordinates([this.props.markers[2], this.props.markers[3]], {
      edgePadding: { top: 100, right: 100, bottom: 100, left: 100 },
      animated: true,
    });
  }

  goToDefaultRegion() {
    this.mapRef.animateToRegion(newRegion, 500);
  }

  recordEvent(e, doctor, i) {
    if (e.persist) {
      e.persist();
    }
    if (e.nativeEvent.action === "marker-press") {
      this.setState({
        region: {
          ...this.state.region,
          latitude: e.nativeEvent.coordinate.latitude,
          longitude: e.nativeEvent.coordinate.longitude,
        }
      }, () => {
        this.setState({
          selectedMarker: i,
        }, () => {
          // this.mapRef.animateToRegion(this.state.region, 500);
          // console.log("ODCTOR PROPS:---", doctor);
          this.props.removeRenderDoctorList();
          this.props.viewDoctor(
            // doctor.name,
            // doctor.qualification,
            // doctor.speciality,
            // doctor.phone || '',
            // doctor.city || '',
            // doctor.coordinates,
            // doctor.medicines,
            doctor.uid,
          );
          Actions.doctorView();
        });
      });
    }

  }

  // onLayout={
  //   () =>
  //     this.mapRef.fitToCoordinates(
  //       this.props.markers,
  //       {
  //         edgePadding: { top: 10, right: 10, bottom: 10, left: 10 },
  //         animated: false
  //       }
  //     )
  // }
  render() {
    return (
      <View>
        <View style={styles.container}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            ref={(ref) => { this.mapRef = ref }}
          >
            {this.props.doctors && this.props.doctors.map((doctor, i) =>
              <Marker
                ref="marker"
                coordinate={doctor.coordinates}
                key={
                  Platform.OS === 'android' && this.props.render ? `${doctor.uid}${Date.now()}` : i
                }
                onPress={(e) => this.recordEvent(e, doctor, i)}
                title={doctor.name}
                identifier={doctor.uid}
                pinColor={this.state.selectedMarker === i ? '#000000' : 'green'}
              >
              </Marker>
            )}
          </MapView>
          {this.renderMapButtons()}
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    width: width,
    // height: height-100,
    height: 450,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.3)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    marginTop: 12,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  callout: {
    width: 100,
  },
};

const mapStateToProps = (state) => {
  var markers = [];
  const { loading, doctors, render } = state.doctors;
  // const doctors = _.map(state.doctors.doctors, (val, uid) => {
  //   return { ...val, uid };
  // });
  if (doctors != null) {
    doctors.map((doctor) => {
      var marker = {
        latitude: doctor.coordinates.latitude,
        longitude: doctor.coordinates.longitude,
      };
      markers.push(marker);
    });
  }
  return { doctors, render, loading, markers, };
};

export default connect(
  mapStateToProps,
  {
    doctorList,
    viewDoctor,
    setActiveRoute,
    removeRenderDoctorList
  }
)(DoctorList);