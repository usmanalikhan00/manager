import React from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { Button, CardSection, Spinner } from './common';
import DoctorPicker from "./DoctorPicker";
import DoctorPickerView from './DoctorPickerView';
import { updateDoctors, setUserDoctors, resetUserDoctors } from "../actions";

class AssignDoctors extends React.Component {

  updateDoctors() {
    this.props.updateDoctors(this.props.doctorsUid, this.props.doctorsData, this.props.uid);
  }

  componentWillUnmount() {
    // console.log(this.props.uids);
    // this.props.setUserDoctors(this.props.uid)
    this.props.resetUserDoctors(this.props.uids, this.props.doctors);
  }


  renderButton() {
    if (this.props.loading) {
      return (
        <Spinner />
      )
    }
    return (
      <Button onPress={() => this.updateDoctors()}>
        Save
      </Button>
    )
  }

  render() {
    return (
      <View>
        <DoctorPicker />
        <DoctorPickerView />
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    doctorsUid: state.assignDoctors.doctorsUid,
    doctorsData: state.assignDoctors.doctorsData,
    uid: state.employeeForm.uid,
    loading: state.assignDoctors.loading,
    uids: state.employeeForm.doctorsUid,
    doctors: state.employeeForm.doctorsData,
  }
};

export default connect(mapStateToProps, { updateDoctors, setUserDoctors, resetUserDoctors })(AssignDoctors);