import React from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { CardSection } from "./common";
import { Actions } from "react-native-router-flux";

class ListItem extends React.Component {

	onPress() {
		Actions.medicineEdit({ medicine: this.props.medicine });
	}

	render() {
		const { name } = this.props.medicine;
		const { textStyle } = styles;
		return (
			<TouchableWithoutFeedback onPress={this.onPress.bind(this)}>
				<View>
					<CardSection>
						<Text
							style={textStyle}
						>
							{name}
						</Text>
					</CardSection>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 18,
		padding: 10
	}
};

export default ListItem;