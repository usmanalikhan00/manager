import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Animated, Marker } from 'react-native-maps';
import { connect } from 'react-redux';
import { changeMapMarker } from '../actions';
import { Spinner, CardSection } from './common';
const { width } = Dimensions.get('window');

class ViewLocationMapWidget extends Component {

  state = {
		coordinates: {
			latitude: 37.77839052733534,
			longitude: -122.4467659369111,
		},
	}

  render() {
    // console.log(this.props);
    if (this.props.coordinates == "") {
      return (
        <View style={styles.container}>
          <Animated
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            // onRegionChangeComplete={(e) => console.log(e)}
            region={{
              latitude: this.state.coordinates.latitude,
              longitude: this.state.coordinates.longitude,
              latitudeDelta: 0.0014851563924480615,
              longitudeDelta: 0.0020116567611694336,
            }}
          >
            <Marker
              coordinate={this.state.coordinates}
            />
          </Animated>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Animated
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          // onRegionChangeComplete={(e) => console.log(e)}
          region={{
            latitude: this.props.coordinates.latitude,
            longitude: this.props.coordinates.longitude,
            latitudeDelta: 0.0014851563924480615,
            longitudeDelta: 0.0020116567611694336,
          }}
        >
          <Marker
            coordinate={this.props.coordinates}
          />
        </Animated>
      </View>
    );
  }
}


const styles = {
  container: {
    width: width - 21.5,
    height: 325,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
};

const mapStateToProps = (state) => {
  return {
    coordinates: state.selectedDoctor.coordinates
  }
}

export default connect(null, { changeMapMarker })(ViewLocationMapWidget);