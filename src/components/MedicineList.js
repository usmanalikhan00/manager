import React from 'react';
import { FlatList } from 'react-native';
import ListItem from './MedicineListItem';
import { connect } from 'react-redux';
import { medicineList, setActiveRoute } from "../actions";
import { Spinner, CardSection } from './common';

class MedicineList extends React.Component {

  componentDidMount() {
    this.props.setActiveRoute('Medicines');
    this.props.medicineList();
  }

  renderContent = () => {
    const { loading, medicines } = this.props;
    if (loading) {
      return (
        <CardSection>
          <Spinner />
        </CardSection>
      );
    }
    else {
      return (
        <FlatList
          data={medicines}
          renderItem={({ item }) => <ListItem medicine={item}></ListItem>}
          keyExtractor={medicine => medicine.uid && medicine.uid}
        />
      );
    }
  }

  render() {
    return (
      this.renderContent()
    );
  }
}

const mapStateToProps = (state) => {
  return {
    medicines: state.medicines.medicines,
    loading: state.medicines.loading,
  }
}

export default connect(mapStateToProps, { medicineList, setActiveRoute })(MedicineList);