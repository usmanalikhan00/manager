import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Animated, Marker } from 'react-native-maps';
import { connect } from 'react-redux';
import { changeMapMarker } from '../actions';
const { width } = Dimensions.get('window');

class FormsMapWidget extends Component {


  componentDidMount() {
    this.props.getCoordinates(this.props.coordinates);
    // this.props.changeMapMarker(this.props.coordinates);
  }

  onMapPress = coordinates => {
    this.props.changeMapMarker(coordinates);
    this.props.getCoordinates(coordinates);
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={this.props.region}
          onPress={(e) => this.onMapPress(e.nativeEvent.coordinate)}
        >
          <Marker
            coordinate={{
              latitude: this.props.coordinates.latitude,
              longitude: this.props.coordinates.longitude,
            }}
          />
        </Animated>
      </View>
    );
  }
}


const styles = {
  container: {
    width: width - 21.5,
    height: 325,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
};

const mapStateToProps = (state) => {
  return {
    region: state.doctorForm.region,
    coordinates: state.doctorForm.coordinates,
  }
}

export default connect(mapStateToProps, { changeMapMarker })(FormsMapWidget);