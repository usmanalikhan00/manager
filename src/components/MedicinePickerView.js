import React from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { Card, CardSection, Chip } from './common';
import Icon from 'react-native-vector-icons/FontAwesome';
import { removeMedicine } from "../actions";

class MedicinePickerView extends React.Component {

  removeMedicine(medicine, i) {
    this.props.removeMedicine(this.props.medicinesUid, this.props.medicinesData, i)
  }

  render() {
    return (
      <View>
        <CardSection
          style={{
            flexWrap: 'wrap'  
          }}
        >
          {this.props.medicinesData && this.props.medicinesData.map((medicine, i) => {
            return (
              <Chip
                key={medicine.uid}
                text={medicine.name}
                view={true}
                onPress={() => this.removeMedicine(medicine, i)}
              />
            );
          })}
        </CardSection>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    medicinesUid: state.assignMedicine.medicinesUid,
    medicinesData: state.assignMedicine.medicinesData,
  }
};

export default connect(mapStateToProps, { removeMedicine })(MedicinePickerView);