import React from 'react';
import { Text, View, Picker } from 'react-native';
import { connect } from 'react-redux';
import { CardSection, Button } from './common';
import { selectMedicine, medicineList } from "../actions";

class MedicinePicker extends React.Component {

  state = {
    value: null,
  }

    
  componentDidMount() {
    this.props.medicineList();
  }

  render() {
    // console.log("PROPS FROM MEDICINE PICKER:--", this.props);
    return (
      <View>
        <CardSection>
          <Text style={styles.pickerLabelStyle}>Medicine Picker</Text>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.state.value}
            onValueChange={value => this.setState({ value })}
          >
            {this.props.medicines && this.props.medicines.map((medicine, i) => {
              return (
                <Picker.Item label={medicine.name} value={medicine.uid} key={i} />
              );
            })}
          </Picker>
        </CardSection>
        <CardSection>
          <Button
            onPress={() =>
              this.props.selectMedicine(this.props.medicinesUid, this.state.value || this.props.medicines[0].uid)
            }
          >
            Select
          </Button>
        </CardSection>
      </View>
    );
  }
}

const styles = {
  pickerLabelStyle: {
    fontSize: 18,
    paddingLeft: 20
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    medicines: state.medicines.medicines,
    medicinesUid: state.assignMedicine.medicinesUid,
    // medicinesData: state.assignMedicine.medicinesData,
  };
}

export default connect(mapStateToProps, { selectMedicine, medicineList })(MedicinePicker);