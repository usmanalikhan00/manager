import React from 'react';
import { connect } from 'react-redux';
import MedicineForm from "./MedicineForm";
import { Card, CardSection, Button, Spinner } from './common';
import * as _ from 'lodash';
import { medicineInputChanged, medicineEdit, medicineFormReset } from '../actions';

class MedicineEdit extends React.Component {

  componentDidMount() {
    _.each(this.props.medicine, (value, prop) => {
			this.props.medicineInputChanged({ prop, value });
		});
  }

  medicineEdit() {
    const {
      name,
      dosage,
      unit,
      type,
      salt,
      price,
      purpose,
    } = this.props;
    this.props.medicineEdit(
      name,
      dosage,
      unit,
      type,
      salt,
      price,
      purpose,
      this.props.medicine.uid,
    )
  }

  renderButton() {
    if (this.props.loading) {
      return (
        <Spinner />
      );
    }
    return (
      <Button onPress={() => this.medicineEdit()}>
        Save
      </Button>
    );
  }

  render() {
    return (
      <Card>
        <MedicineForm {...this.props} />
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
    loading,
  } = state.medicineForm;

  return {
    name,
    dosage,
    unit,
    type,
    salt,
    price,
    purpose,
    loading,
  };
}


export default connect(mapStateToProps, { medicineInputChanged, medicineEdit, medicineFormReset })(MedicineEdit);