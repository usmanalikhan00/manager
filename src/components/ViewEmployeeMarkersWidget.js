import React from 'react';
import { Text, View, Dimensions, Platform, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE, Animated, Marker, Callout } from 'react-native-maps';
import { } from "../actions";
import { CardSection, Spinner } from "./common";
import Geolocation from 'react-native-geolocation-service';
import getDistance from 'geolib/es/getDistance';
import convertDistance from 'geolib/es/convertDistance';
const { width, height } = Dimensions.get('window');
const DEFAULT_PADDING = { top: 10, right: 10, bottom: 10, left: 10 };
const newRegion = {
  latitude: 31.582045,
  longitude: 74.329376,
  latitudeDelta: 0.015,
  longitudeDelta: 0.0121,
}

class ViewMarkerWidget extends React.Component {

  state = {
    region: {
      latitude: 31.582045,
      longitude: 74.329376,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    },
    coordinates: {
      latitude: 31.582045,
      longitude: 74.329376,
    },
    selectedMarker: null,
  }

  mapRef = null;

  recordEvent(e, doctor, i) {
    if (e.persist) {
      e.persist();
    }
    if (e.nativeEvent.action === "marker-press") {
      this.setState({
        region: {
          ...this.state.region,
          latitude: e.nativeEvent.coordinate.latitude,
          longitude: e.nativeEvent.coordinate.longitude,
        }
      }, () => {
        this.setState({
          selectedMarker: i,
        }, () => {
          // this.mapRef.animateToRegion(this.state.region, 500);
          // console.log("ODCTOR PROPS:---", doctor);
          Geolocation.getCurrentPosition(
            position => {
              const location = position;
              this.setState({ location }, () => {
                this.setState({
                  region: {
                    ...this.state.region,
                    latitude: this.state.location.coords.latitude,
                    longitude: this.state.location.coords.longitude,
                  },
                  coordinates: {
                    ...this.state.coordinates,
                    latitude: this.state.location.coords.latitude,
                    longitude: this.state.location.coords.longitude,
                  }
                }, () => {
                  console.log(convertDistance(getDistance(
                    doctor.coordinates,
                    this.state.location.coords
                  ), 'km'));
                  // convertDistance(getDistance(
                  //   doctor.coordinates,
                  //   this.state.location.coords
                  // ), 'km');
                });
              });
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, showLocationDialog: true }
          );
          // console.log("COM");
        });
      });
    }

  }

  renderMapButtons() {
    if (this.props.loading) {
      return (
        <CardSection style={styles.container}>
          <Spinner size="large" />
        </CardSection>
      );
    }
    return (
      <View style={styles.btnContainer}>
        <TouchableOpacity
          onPress={() => this.goToDefaultRegion()}
          style={[styles.bubble, styles.button]}
        >
          <Text>Set Markers</Text>
        </TouchableOpacity>
      </View>
    );
  }

  goToDefaultRegion() {
    this.mapRef.animateToRegion(newRegion, 500);
  }

  render() {
    console.log(this.props.doctors);
    return (
      <View>
        <View style={styles.container}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            ref={(ref) => { this.mapRef = ref }}
          >
            {this.props.doctors && this.props.doctors.map((doctor, i) =>
              <Marker
                ref="marker"
                coordinate={doctor.coordinates}
                // key={
                //   Platform.OS === 'android' && this.props.render ? `${doctor.uid}${Date.now()}` : i
                // }
                key={i}
                onPress={(e) => this.recordEvent(e, doctor, i)}
                title={doctor.name}
                identifier={doctor.uid}
                pinColor={this.state.selectedMarker === i ? '#000000' : 'green'}
              >
              </Marker>
            )}
          </MapView>
          {this.renderMapButtons()}
        </View>
      </View>
    )
  }
};

const styles = {
  container: {
    width: width,
    height: 300,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.3)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    marginTop: 12,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  callout: {
    width: 100,
  },
};

const mapStateToProps = state => {

  console.log(state)

  let employeeDoctors = state.employeeDoctors.doctors;
  let allDoctors = state.doctors.doctors;
  let markers = [];
  let doctors = [];

  switch (employeeDoctors, allDoctors) {
    case (employeeDoctors != null):
      doctors = employeeDoctors;
      doctors.map((doctor) => {
        var marker = {
          latitude: doctor.coordinates.latitude,
          longitude: doctor.coordinates.longitude,
        };
        markers.push(marker);
      });
      return {
        doctors,
        markers,
      };
    case (allDoctors != null):
      doctors = allDoctors;
      doctors.map((doctor) => {
        var marker = {
          latitude: doctor.coordinates.latitude,
          longitude: doctor.coordinates.longitude,
        };
        markers.push(marker);
      });
      return {
        doctors,
        markers,
      };
    default:
      return {
        doctors,
        markers,
      };
  }

  // console.log(allDoctors, employeeDoctors, markers);

  // if (doctors != null) {
  // }

}

export default connect(mapStateToProps, {})(ViewMarkerWidget);