import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Input, CardSection } from './common';
import { connect } from 'react-redux';
import {
	nameChanged,
	specialityChanged,
	qualificationChanged,
	phoneChanged, 
	inputChanged,
} from '../actions';

class DoctorForm extends Component {

	nameChanged = (value) => {
		this.props.nameChanged(value);
	}
	qualificationChanged = (value) => {
		this.props.qualificationChanged(value);
	}
	specialityChanged = (value) => {
		this.props.specialityChanged(value);
	}
	phoneChanged = (value) => {
		this.props.phoneChanged(value);
	}

	render() {
		return (
			<View>
				<CardSection>
					<Input
						onChangeText={this.nameChanged}
						value={this.props.name}
						label="Name"
						placeholder="John Doe"
					/>
				</CardSection>
				<CardSection>
					<Input
						onChangeText={this.specialityChanged}
						value={this.props.speciality}
						label="Speciality"
						placeholder="Peads, Medicine, etc."
					/>
				</CardSection>
				<CardSection>
					<Input
						onChangeText={this.qualificationChanged}
						value={this.props.qualification}
						label="Qualification"
						placeholder="M.B.B.S, F.R.C.P.S, etc."
					/>
				</CardSection>
				<CardSection>
					<Input
						onChangeText={this.phoneChanged}
						value={this.props.phone}
						label="Phone"
						placeholder="03xxxxxxxxxxx"
					/>
				</CardSection>
				<CardSection>
					<Input
						onChangeText={(value) => this.props.inputChanged({prop: 'city', value})}
						value={this.props.city}
						label="City"
						placeholder="Lahore"
					/>
				</CardSection>
			</View>
		);
	}
}

const mapStateToProps = (state) => {
	const {
		name,
		speciality,
		qualification,
		phone,
		city,
	} = state.doctorForm;

	return {
		name,
		speciality,
		qualification,
		phone,
		city,
	};

}

export default connect(
	mapStateToProps,
	{
		nameChanged,
		qualificationChanged,
		specialityChanged,
		phoneChanged,
		inputChanged,
	}
)(DoctorForm);