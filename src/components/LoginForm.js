import React, { Component } from 'react';
import { Text, View } from 'react-native'; 
import { Card, CardSection, Input, Button, Spinner } from './common';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';

class LoginForm extends Component {
	
	onEmailChange(text) {
		this.props.emailChanged(text);
	}

	onPasswordChange(text) {
		this.props.passwordChanged(text);
	}

	onLogin() {
		const { email, password } = this.props;
		this.props.loginUser({ email, password });
	}
	
	renderError() {
		if (this.props.error) {
			return (
				<View style={{backgroundColor: '#fff'}}>
					<Text style={styles.errorTextStyle}>
						{this.props.error}
					</Text>
				</View>
			);
		}
	}

	renderButton() {
		if (this.props.loading){
			return (
				<Spinner size="large" />
			);
		}

		return (
			<Button onPress={this.onLogin.bind(this)}>
				Log In
			</Button>
		);
	}


	render() {
		return (
			<Card>
				<CardSection>
					<Input 
						value={this.props.email}
						onChangeText={this.onEmailChange.bind(this)}
						label="Email"
						placeholder="user@email.com"
					/>
				</CardSection>
				
				<CardSection>
					<Input
						value={this.props.password} 
						onChangeText={this.onPasswordChange.bind(this)}
						label="Passsword"
						placeholder="password"
						secureTextEntry
					/>
				
				</CardSection>
				
				{this.renderError()}

				<CardSection>
					{this.renderButton()}
				</CardSection>
				
			</Card>
		);
	}
}

const mapStateToProps = (state) => {
	return { 
		email: state.auth.email, 
		password: state.auth.password,
		error: state.auth.error,
		loading: state.auth.loading 
	};
};


const styles = {
	errorTextStyle: {
		fontSize: 18,
		color: 'red',
		alignSelf: 'center'
	}
}
export default connect(
	mapStateToProps, 
	{	
		emailChanged, 
		passwordChanged, 
		loginUser
	}
)(LoginForm);

