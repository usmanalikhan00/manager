import React from 'react';
import { Text, View, Picker } from 'react-native';
import { connect } from 'react-redux';
import { employeesList, getEmployeeDoctors } from '../actions';
import { CardSection } from './common';

class EmployeePicker extends React.Component {

  state = {
    value: null
  };

  componentDidMount() {
    this.props.employeesList();
    this.props.getEmployeeDoctors(this.props.employees[0].uid);
  }

  render() {
    // console.log(this.props);
    return (
      <CardSection>
        <Text style={{alignSelf: 'center', fontSize: 18, padding: 5}}>
          Pick Employee
        </Text>
        <Picker
          style={{ flex: 1 }}
          selectedValue={this.state.value}
          onValueChange={value => this.setState({ value },
            () => { this.props.getEmployeeDoctors(value || this.props.employees && this.props.employees[i].uid) })}
        >
          {this.props.employees && this.props.employees.map((employee, i) => {
            return (
              <Picker.Item label={employee.name} value={employee.uid} key={i} />
            );
          })}
        </Picker>
      </CardSection>
    );
  }
}

const mapStateToProps = state => {
  const { employees } = state.employees;
  const { uid } = state.employeeForm;
  return {
    employees,
    uid
  };
};

export default connect(mapStateToProps, { employeesList, getEmployeeDoctors })(EmployeePicker);