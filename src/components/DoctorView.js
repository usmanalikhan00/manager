import React, { Component } from 'react';
import { Text, View, Dimensions, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { viewDoctor, viewDoctorComplete, clearSelectedDoctor, doctorList } from '../actions';
import ViewLocationMapWidget from './ViewLocationMapWidget';
import { Spinner, CardSection, Card, ListItem, Chip } from './common';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
const { width } = Dimensions.get('window');

class DoctorView extends Component {

  componentDidMount() {
    // const { name, speciality, qualification, phone, city, coordinates } = this.props.doctor;
    // this.props.viewDoctorComplete();
    // this.props.viewDoctor(name, qualification, speciality, phone, city || '', coordinates);
  }

  goToEdit = () => {
    const {
      name,
      speciality,
      qualification,
      phone,
      city,
      coordinates,
      medicinesUid,
      medicinesData,
      uid
    } = this.props;
    Actions.doctorEdit({
      doctor: {
        name,
        speciality,
        qualification,
        phone,
        city,
        coordinates,
        medicinesUid,
        medicinesData,
        uid
      }
    })
  }

  renderContent() {
    if (this.props.loading) {
      return (
        <CardSection style={styles.loadingContainer}>
          <Spinner />
        </CardSection>
      );
    }
    return (
      <ScrollView>
        <Card>
          <CardSection>
            <ViewLocationMapWidget coordinates={this.props.coordinates} />
            <View style={styles.editBtnContainer}>
              <View style={styles.editBtn}>
                <Icon onPress={() => this.goToEdit()} name="ios-create" size={20} color="#fff" />
              </View>
            </View>
          </CardSection>
          <CardSection style={{ padding: 8 }}>
            <Text style={{ fontSize: 18 }}>Information</Text>
          </CardSection>
          <CardSection>
            <ListItem label="Name" value={this.props.name} />
          </CardSection>
          <CardSection>
            <ListItem label="Qualification" value={this.props.qualification} />
          </CardSection>
          <CardSection>
            <ListItem label="Speciality" value={this.props.speciality} />
          </CardSection>
          <CardSection>
            <ListItem label="Phone" value={this.props.phone} />
          </CardSection>
          <CardSection>
            <ListItem label="City" value={this.props.city} />
          </CardSection>
          <CardSection style={{
            display: 'flex', flexDirection: 'row',
            justifyContent: 'space-between', padding: 8,
          }}>
            <Text style={{ fontSize: 18 }}>Medicines</Text>
            <TouchableWithoutFeedback onPress={() => Actions.assignDoctorMedicine()}>
              <FontAwesome name="plus-square" size={26} />
            </TouchableWithoutFeedback>
          </CardSection>
          <CardSection>
            {this.props.medicinesData && this.props.medicinesData.map((medicine) => {
              return (
                <Chip
                  key={medicine.uid}
                  text={medicine.name}
                  view={false}
                />
              );
            })}
          </CardSection>

        </Card>
      </ScrollView>
    );
  }

  componentWillUnmount() {
    // this.props.doctorList();
  }

  render() {
    console.log("PROPS FROM VIEW DOCTOR:--", this.props);
    return (
      this.renderContent()
    );
  }
}

const mapStateToProps = (state) => {
  const {
    name,
    speciality,
    qualification,
    phone,
    city,
    coordinates,
    medicinesUid,
    medicinesData,
    uid,
    loading,
  } = state.selectedDoctor;
  return {
    name,
    speciality,
    qualification,
    phone,
    city,
    coordinates,
    medicinesUid,
    medicinesData,
    uid,
    loading,
  };
};

const styles = {
  editBtn: {
    left: 12,
    top: 8,
    position: 'absolute',
  },
  editBtnContainer: {
    borderRadius: 25,
    height: 40,
    width: 40,
    backgroundColor: '#4F8EF7',
    position: 'absolute',
    zIndex: 9999,
    bottom: 15,
    right: 8,
  },
  loadingContainer: {
    width: width,
    height: 325,
    alignItems: 'center',
  },
}

export default connect(mapStateToProps, { viewDoctor, clearSelectedDoctor, doctorList })(DoctorView);