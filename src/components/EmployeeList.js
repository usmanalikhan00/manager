import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { employeesList, checkLogged, setActiveRoute } from '../actions';
import * as _ from 'lodash';
import ListItem from './EmployeeListItem';
import { Spinner, CardSection } from './common';

class EmployeeList extends Component {
	
	componentDidMount() {
		this.props.setActiveRoute('Employees');
		this.props.employeesList(this.props.user);
	}

	renderContent() {
		if (this.props.loading){
			return (
				<CardSection>
					<Spinner size="large" />
				</CardSection>
			);
		}
		return (
			<FlatList
			  data={this.props.employees}
			  renderItem={({item}) => <ListItem employee={item}></ListItem>}
			  keyExtractor={employee => employee.uid}
			/>
		);
	}

	componentWillUnmount() {
		this.props.setActiveRoute('');
	}

	render() {
		// console.log("Props From employees List:--\n", this.props);
		return (
			<View>
				{this.renderContent()}
			</View>
		);
	}
}

const mapStateToProps = state => {
	const { loading, employees } = state.employees;
	const { user } = state.auth;
	// const employees = _.map(state.employees.employees, (val, uid) => {
	// 	return { ...val, uid };
	// });
	return { employees, loading, user };
};

export default connect(mapStateToProps, { employeesList, checkLogged, setActiveRoute })(EmployeeList);