import React, { Component } from 'react';
import { Text, View } from 'react-native';
import DoctorForm from './DoctorForm';
import { connect } from 'react-redux';
import { inputChanged, doctorReset, viewDoctor, doctorEdit, doctorList, setRenderDoctorList } from '../actions';
import FormsMapWidget from './FormsMapWidget';
import { CardSection, Card, Button, Spinner, Confirm } from './common';
import * as _ from 'lodash';
import { Actions } from 'react-native-router-flux';

class DoctorEdit extends Component {

	state = {
		coordinates: {
			latitude: 37.77839052733534,
			longitude: -122.4467659369111,
		},
		showModal: false
	}

	componentDidMount() {
		_.each(this.props.doctor, (value, prop) => {
			this.props.inputChanged({ prop, value });
		});
	}
	
	setCoordinates = (coordinates) => {
		this.setState({
			coordinates: {
				...this.state.coordinates,
				latitude: coordinates.latitude,
				longitude: coordinates.longitude,
			}
		});
	}

	onSaveDoctor() {
		const { name, speciality, qualification, city, phone, coordinates, medicinesUid, medicinesData } = this.props;
		const { uid } = this.props.doctor;
		this.props.doctorEdit(
			name,
			qualification,
			speciality,
			phone,
			city,
			coordinates,
			medicinesUid,
			medicinesData,
			uid,
		);
		this.props.doctorList();
		this.props.setRenderDoctorList();
		// Actions.pop();
		// this.setState({ showModal: !this.state.showModal })
	}
	
	renderButton() {
		if (this.props.loading) {
			return (
				<Spinner />
				);
			}
			return (
				<Button onPress={() => this.onSaveDoctor()}>
				Save
			</Button>
		);
		
	}

	componentWillUnmount() {
		this.props.doctorReset();
	}
	
	onAccept() {
		this.setState({ showModal: !this.state.showModal }, () => {
			Actions.pop();
		});
	}

	render() {
		return (
			<Card>
				<DoctorForm {...this.props} />
				<CardSection>
					<FormsMapWidget
						getCoordinates={this.setCoordinates}
					/>
				</CardSection>
				<CardSection>
					{this.renderButton()}
				</CardSection>
				{/* <Confirm
					visible={this.state.showModal}
					onAccept={this.onAccept.bind(this)}
				>
					Updated Successfully..
				</Confirm> */}

			</Card>
		)
	}

}

const mapStateToProps = (state) => {
	// const { render } = state.doctors;
	const { name, speciality, qualification, city, phone, coordinates, loading } = state.doctorForm;
	const { medicinesUid, medicinesData } = state.selectedDoctor;
	return { name, speciality, qualification, city, phone, coordinates, medicinesUid, medicinesData, loading };
}

export default connect(mapStateToProps, { inputChanged, doctorReset, viewDoctor, doctorEdit, doctorList, setRenderDoctorList })(DoctorEdit);