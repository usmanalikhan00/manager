import React, { Component } from 'react';
import { Text, 
				 TouchableWithoutFeedback, 
				 View, 
				 AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { logOutUser } from '../actions';

class ListItem extends Component {

	onPress() {
		const { name } = this.props.item;

		if (name === 'Doctors') {
			Actions.doctorList();
		} else if (name === 'Users') {
			Actions.users();
		} else if (name === 'Employees') {
			Actions.employeeList();
		} else if (name === 'Medicines') {
			Actions.medicineList();
		} else if (name === 'Logout') {
			// this.removeItemValue('LoggedUser');
			// Actions.auth();
			this.props.logOutUser();
		} else {
			Actions.areas();
		}
	}

	logOut = () => {
    this.props.logOutUser();
    // Actions.main();
  }

	async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch(exception) {
      return false;
    }
  }

	render() {
		const { name } = this.props.item;
		const { textStyle, textStyleActive } = styles;
		return (
			<TouchableWithoutFeedback onPress={this.onPress.bind(this)}>
				<View style={styles.container}>
					<View style={styles.innerContainer}>
						<Icon name="ios-person" size={28} color="#4F8EF7" />
					</View>
					<View style={styles.innerContainer}>
							<Text 
								style={this.props.activeRoute === name ? textStyleActive : textStyle}
							>
								 {name}
							</Text>
					</View>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	container: {
		flex: 1, 
		flexDirection: 'row'
	},
	innerContainer: {
		justifyContent: 'center', 
		padding: 5
	},
	textStyle: {
		fontSize: 15,
		padding: 10
	},
	textStyleActive: {
		fontSize: 15,
		padding: 10,
		color: 'red'
	}

};

const mapStateToProps = (state) => {
	return {
		activeRoute: state.activeRoute.activeRoute,
	};
}

export default connect(mapStateToProps, { logOutUser })(ListItem);