import React from 'react';
import { Text, View } from 'react-native';
import { Input, Card, CardSection } from './common';

class NavBar extends React.Component {
  render() {
    return (
      <CardSection style={{ marginTop: 50 }}>
        <Input
          label="Search"
          placeholder="Keyword..."
        />
      </CardSection>
    );
  }
}

export default NavBar;