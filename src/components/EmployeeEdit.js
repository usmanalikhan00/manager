import React, { Component } from 'react';
import { ScrollView, Text, TouchableWithoutFeedback } from 'react-native';
import { Chip, Card, CardSection, Button, Confirm, Spinner } from './common';
import EmployeeForm from './EmployeeForm';
import { connect } from 'react-redux';
import {
	employeeUpdate,
	employeeEdit, employeeRemove,
	employeeReset, setUserDoctors,
	viewDoctor
} from '../actions';
import * as _ from 'lodash';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

class EmployeeEdit extends Component {

	state = {
		showModal: false
	};

	async componentDidMount() {
		console.log("FROM EDIT PAGEs:--", this.props.employee);
		await _.each(this.props.employee, (value, prop) => {
			this.props.employeeUpdate({ prop, value });
		});
		this.props.setUserDoctors(this.props.uid);
	}

	componentWillUnmount() {
		this.props.employeeReset();
	}

	onButtonPress() {
		const { name, phone, shift } = this.props;
		this.props.employeeEdit({ name, phone, shift, uid: this.props.employee.uid })
	}

	textSchedule() {

	}


	onAccept() {
		const { uid } = this.props.employee;
		this.props.employeeRemove({ uid });
	}

	onDecline() {
		this.setState({ showModal: !this.state.showModal });
	}

	renderSaveButton() {
		if (this.props.loading) {
			return (
				<Spinner />
			);
		}
		return (
			<Button onPress={this.onButtonPress.bind(this)}>
				Save Changes
			</Button>
		);
	}

	onViewPress(doctor) {
		this.props.viewDoctor(
			doctor.uid,
		);
		Actions.doctorView();
	}

	render() {
		// console.log(this.props);
		return (
			<ScrollView>
				<Card>
					<EmployeeForm {...this.props} />
					<CardSection style={{
						flexDirection: 'row',
						justifyContent: 'space-between', padding: 10
					}}>
						<Text style={{ fontSize: 17, fontWeight: '500' }}>Assigned Doctors</Text>
						<TouchableWithoutFeedback onPress={() => Actions.assignDoctor()}>
							<FontAwesome name="edit" color="#4F8EF7" size={26} />
						</TouchableWithoutFeedback>
					</CardSection>
					<CardSection style={{
						flexDirection: 'row',
						flexWrap: 'wrap',
					}}>
						{this.props.loader ? <Spinner /> : this.props.doctorsData && this.props.doctorsData.map((doctor, i) => {
							return (
								<Chip
									view={false}
									// onViewPress={() => this.onViewPress(doctor)}
									key={i} text={doctor.name}
								/>
							);
						})}
					</CardSection>
					<CardSection>
						{this.renderSaveButton()}
					</CardSection>
					<CardSection>
						<Button onPress={this.textSchedule.bind(this)}>
							Text Schedule
					</Button>
					</CardSection>
					<CardSection>
						<Button onPress={() => this.setState({ showModal: !this.state.showModal })}>
							Fire Employee
					</Button>
					</CardSection>
					<Confirm
						visible={this.state.showModal}
						onAccept={this.onAccept.bind(this)}
						onDecline={this.onDecline.bind(this)}
					>
						Are You Sure?
				</Confirm>
				</Card>
			</ScrollView>
		);
	}
}

const mapStateToProps = (state) => {
	const { name, phone, shift, loading, uid, doctors, doctorsUid, doctorsData } = state.employeeForm;
	return { name, phone, shift, loading, uid, doctors, doctorsUid, doctorsData, loader: state.assignDoctors.loading };
};

export default connect(
	mapStateToProps,
	{
		employeeUpdate,
		employeeEdit,
		employeeRemove,
		employeeReset,
		viewDoctor,
		setUserDoctors
	}
)(EmployeeEdit);
