import React from 'react';
import { View, Dimensions } from 'react-native';
import { CardSection, Spinner } from './common';
import ViewMarkersWidget from "./ViewEmployeeMarkersWidget";
import EmployeePicker from "./EmployeePicker";
import { connect } from 'react-redux';
import { getEmployeeDoctors } from "../actions";
const { width, height } = Dimensions.get('window');

class AssignedDoctorsModal extends React.Component {

  // componentDidMount() {
  //   this.props.getEmployeeDoctors(this.props.employees[0].uid);
  // }

  renderContent() {
    if (this.props.loading) {
      return (
        <CardSection style={styles.container}>
          <Spinner />
        </CardSection>
      )
    }
    return (
      <CardSection>
        <ViewMarkersWidget />
      </CardSection>
    );
  }

  render() {
    return (
      <View>
        <EmployeePicker />
        {this.renderContent()}
      </View>
    );
  }
}

const styles = {
  container: {
    width: width,
    height: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
}

const mapStateToProps = state => {
  const { loading, employees } = state.employeeDoctors;
  return { loading, employees };
}

export default connect(mapStateToProps, { getEmployeeDoctors })(AssignedDoctorsModal);