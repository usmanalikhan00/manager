import { Actions } from 'react-native-router-flux';

export const EMPLOYEE_ADD_ACTION = () => Actions.employeeCreate();
export const DOCTOR_ADD_ACTION = () => Actions.doctorCreate();
export const MEDICINE_ADD_ACTION = () => Actions.medicineCreate();
// export const EMPLOYEE_ADD_ACTION = () => Actions.employeeCreate();
// export const EMPLOYEE_ADD_ACTION = () => Actions.employeeCreate();