import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import { 
	EMAIL_CHANGED,
	PASSWORD_CHANGED,
	LOGIN_USER_SUCCESS, 
	LOGIN_USER_FAILURE,
	LOGIN_USER,
	SET_ACTIVE_ROUTE, 
	LOGGED_USER 
} from './types';
// import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


export const setActiveRoute = (route) => {
	return {
		type: SET_ACTIVE_ROUTE,
		payload: route
	};
}

export const emailChanged = (text) => {
	return {
		type: EMAIL_CHANGED,
		payload: text
	};
};

export const passwordChanged = (text) => {
	return {
		type: PASSWORD_CHANGED,
		payload: text
	};
};

export const loginUser = ({ email, password }) => {
	return (dispatch) => {
		dispatch({ type: LOGIN_USER });
		firebase.auth().signInWithEmailAndPassword(email, password)
			.then(user => {
				loginUserSuccess(dispatch, user);
			})
			.catch(() => {
				firebase.auth().createUserWithEmailAndPassword(email, password)
					.then(user => loginUserSuccess(dispatch, user))
					.catch(() => loginUserFailure(dispatch));
			});
	};
};


const loginUserSuccess = (dispatch, user) => {
	this._storeLoggedUser(user);
	// this._retrieveLoggedUser();
	dispatch({ 
		type: LOGIN_USER_SUCCESS, 
		payload: user  
	});
	Actions.employeeList();
}

_storeLoggedUser = async user => {
	try {
		await AsyncStorage.setItem('LoggedUser', JSON.stringify(user));
	} catch (error) {
		// Error saving data
	}
};

_retrieveLoggedUser = async () => {
	try {
		const value = await AsyncStorage.getItem('LoggedUser');
		if (value !== null) {
			// console.log("ASYNC STORAGE VALUE:--", JSON.parse(value));
		}
	} catch (error) {
		// Error retrieving data
	}
};

export const checkLogged = () => {
	return async (dispatch) => {
		const value = await AsyncStorage.getItem('LoggedUser');
		// console.log("Value from local storage 22:--", value)
		dispatch({
			type: LOGGED_USER,
			payload: JSON.parse(value)
		})
	}	
}

export const logOutUser = () => {
	return (dispatch) => {
		firebase.auth().signOut().then(async function(){
			await AsyncStorage.removeItem('LoggedUser')
			dispatch({
				type: LOGGED_USER,
				payload: null
			});
		}).catch(function(err){
			console.log(err);
		});
	};
}

const loginUserFailure = (dispatch) => {
	dispatch({ 
		type: LOGIN_USER_FAILURE
	});
}

