// AUTHENTICATION ACTIONS
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAILURE = 'login_user_failure';
export const LOGIN_USER = 'login_user';
export const LOGGED_USER = 'logged_in_user';
// EMPLOYEES ACTIONS
export const EMPLOYEE_UPDATE = 'employee_update';
export const EMPLOYEE_CREATE = 'employee_create';
export const EMPLOYEE_EDIT = 'employee_edit';
export const EMPLOYEE_REMOVE = 'employee_remove';
export const EMPLOYEE_CREATE_SPINNER = 'employee_create_spinner';
export const EMPLOYEE_REMOVE_SPINNER = 'employee_remove_spinner';
export const EMPLOYEES_LIST_SUCCESS = 'employees_list_success';
export const EMPLOYEES_LIST_SPINNER = 'employees_list_spinner';
// DOCTORS ACTIONS
export const VIEW_DOCTOR = 'view_doctor';
export const DOCTOR_UPDATED = 'doctor_updated';
export const VIEW_DOCTOR_SPINNER = 'view_doctor_spinner';
export const DOCTOR_LIST_SUCCESS = 'doctor_list_success';
export const DOCTOR_LIST_SPINNER = 'doctor_list_spinner';
export const DOCTOR_INPUT_CHANGED = 'doctor_input_chaned';
export const DOCTOR_NAME_CHANGED = 'doctor_name_chaned';
export const DOCTOR_SPECIALITY_CHANGED = 'doctor_speciality_changed';
export const DOCTOR_QUALIFICATION_CHANGED = 'doctor_qualification_changed';
export const DOCTOR_PHONE_CHANGED = 'doctor_phone_changed';
export const DOCTOR_CREATE_SPINNER = 'doctor_create_spinner';
export const DOCTOR_REMOVE_SPINNER = 'doctor_remove_spinner';
export const DOCTOR_INITIAL_STATE = 'doctor_initial_state';
export const CHANGE_FORM_MAP_MARKER = 'change_map_marker';
export const CLEAR_SELECTED_DOCTOR = 'clear_selected_doctor';
// MEDICINE ACTIONS
export const MEDICINE_CREATE_SPINNER = 'medicine_create_spinner';
export const MEDICINE_REMOVE_SPINNER = 'medicine_remove_spinner';
export const MEDICINE_FORM_RESET = 'medicine_form_reset';
export const MEDICINE_INPUT_CHANGED = 'medicine_input_changed';
export const MEDICINE_CREATE_LIST_SPINNER = 'medicine_create_list_spinner';
export const MEDICINE_LIST_SUCCESS = 'medicine_list_success';


// DRAWER ACTIONS
export const SET_ACTIVE_ROUTE = 'set_active_route';
export const SET_MEDICINES = 'set_medicines';
export const CLEAR_DOCTOR_SPINNER = 'clear_doctor_spinner';
export const SET_RENDER_DOCTOR_LIST = 'set_render_doctor_list';
export const REMOVE_RENDER_DOCTOR_LIST = 'remove_render_doctor_list';

export const SET_SELECTED_MEDICINE_UID = 'set_selected_medicine_uid';
export const SET_SELECTED_MEDICINE_DATA = 'set_selected_medicine_data';
export const REMOVE_SELECTED_MEDICINE_UID = 'remove_selected_medicine_uid';
export const REMOVE_SELECTED_MEDICINE_DATA = 'remove_selected_medicine_data';

// OTHERS 
export const REMOVE_SELECTED_DOCTOR_DATA = 'remove_selected_doctor_data';
export const SET_SELECTED_DOCTOR_DATA = 'set_selected_doctor_data';
export const SET_SELECTED_DOCTOR_UID = 'set_selected_doctor_uid';
export const SET_SELECTED_USER_DOCTORS = 'set_selected_user_doctors';
export const SET_USER_FORM_DOCTORS = 'set_user_form_doctors';
export const RESET_USER_FORM_DOCTORS = 'reset_user_form_doctors';
export const ADD_USER_FORM_DOCTOR_SPINNER = 'add_user_form_doctor_spinner';
export const REMOVE_USER_FORM_DOCTOR_SPINNER = 'remove_user_form_doctor_spinner';
export const GET_EMPLOYEE_DOCTORS = 'get_employee_doctors';
export const EMPLOYEE_DOCTORS_SPINNER_CREATE = 'get_employee_doctors_spinner_create';
export const EMPLOYEE_DOCTORS_SPINNER_REMOVE = 'get_employee_doctors_spinner_remove';