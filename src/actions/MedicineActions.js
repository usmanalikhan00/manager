import firebase, { firestore } from 'firebase';

import {
  MEDICINE_CREATE_SPINNER,
  MEDICINE_REMOVE_SPINNER,
  MEDICINE_FORM_RESET,
  MEDICINE_INPUT_CHANGED,
  MEDICINE_CREATE_LIST_SPINNER,
  MEDICINE_LIST_SUCCESS,
} from './types';
import { Actions } from 'react-native-router-flux';

export const medicineInputChanged = ({ prop, value }) => {
  return {
    type: MEDICINE_INPUT_CHANGED,
    payload: { prop, value },
  };
}


export const medicineCreate = (name, salt, dosage, unit, type, price, purpose) => {
  var db = firebase.firestore();
  return async (dispatch) => {
    dispatch({ type: MEDICINE_CREATE_SPINNER });
    await db.collection('medicines').add({
      name: name,
      salt: salt,
      dosage: dosage,
      unit: unit,
      type: type,
      price: price,
      purpose: purpose,
      company: firebase.auth().currentUser.uid,
    }).then(() => {
      dispatch({ type: MEDICINE_REMOVE_SPINNER });
      Actions.medicineList({ type: 'reset' });
    }).catch((err) => {
      console.log(err);
    })
  }
}

export const medicineList = () => {
  var db = firebase.firestore();
  return async (dispatch) => {
    dispatch({ type: MEDICINE_CREATE_LIST_SPINNER });
    var results = []
    var user = firebase.auth().currentUser
    await db.collection("medicines").where("company", "==", user.uid).get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        var medicine = doc.data();
        medicine.uid = doc.id
        results.push(medicine);
      });
      dispatch({ type: MEDICINE_LIST_SUCCESS, payload: results });
    })
    .catch(function (error) {
      console.log("Error getting documents: ", error);
    });
    
  }
}

export const medicineEdit = (name, dosage, unit, type, salt, price, purpose, uid) => {
  var db = firebase.firestore();
  var docArray = []
  db.collection('doctors')
    .where("medicines", "array-contains", uid).get().then(function (doc) {
      console.log(doc);
      doc.forEach(function (doc) {
        var doc = doc.data();
        doc.uid = doc.id;
        docArray.push(doc);
      });
      console.log(docArray);
    })
  return async (dispatch) => {
    dispatch({ type: MEDICINE_CREATE_SPINNER });
    await db.collection("medicines").doc(uid).update({
      name: name,
      dosage: dosage,
      unit: unit,
      type: type,
      salt: salt,
      price: price,
      purpose: purpose,
    }).then(function () {
      dispatch({ type: MEDICINE_REMOVE_SPINNER });
      Actions.medicineList({ type: 'reset' });
    }).catch(function (error) {
      console.log("Error getting documents: ", error);
    });

  }
}

export const medicineFormReset = () => {
  return { type: MEDICINE_FORM_RESET };
}

