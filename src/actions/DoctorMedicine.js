import {
  SET_SELECTED_MEDICINE_UID,
  SET_SELECTED_MEDICINE_DATA,
  REMOVE_SELECTED_MEDICINE_UID,
  REMOVE_SELECTED_MEDICINE_DATA,
  SET_MEDICINES,
  VIEW_DOCTOR,
  VIEW_DOCTOR_SPINNER,
  CLEAR_DOCTOR_SPINNER,
} from "./types";
import firebase from 'firebase';

export const selectMedicine = (uids, uid) => {
  var db = firebase.firestore();
  return async (dispatch) => {
    if (uids.indexOf(uid) < 0) {
      var array = uids;
      array.push(uid);
      // setMedicineData(dispatch, array);
      dispatch({
        type: SET_SELECTED_MEDICINE_UID,
        payload: array,
      });
      await db.collection('medicines').doc(uid).get().then(function (doc) {
        var medicine = doc.data();
        medicine.uid = doc.id;
        dispatch({
          type: SET_SELECTED_MEDICINE_DATA,
          payload: medicine,
        });
      });
    };
  }
}


export const removeMedicine = (medicinesUid, medicinesData, index) => {
  return dispatch => {
    let medicinesUidCopy = [];
    let medicinesDataCopy = [];

    for (let i = 0; i < medicinesUid.length; i++) {
      if (i != index) {
        medicinesUidCopy.push(medicinesUid[i]);
        medicinesDataCopy.push(medicinesData[i]);
      }
    }

    dispatch({
      type: REMOVE_SELECTED_MEDICINE_UID,
      payload: medicinesUidCopy
    });
    dispatch({
      type: REMOVE_SELECTED_MEDICINE_DATA,
      payload: medicinesDataCopy
    });
  }
}


export const updateMedicine = (uid, medicinesUid, medicinesData) => {
  var db = firebase.firestore();
  return async (dispatch) => {
    dispatch({ type: VIEW_DOCTOR_SPINNER });
    await db.collection("doctors").doc(uid).update({
      medicines: medicinesUid,
    }).then(function () {
      db.collection("doctors").doc(uid).get().then(function (doc) {
        const { name, qualification, speciality, phone, city, coordinates, } = doc.data();
        dispatch({
          type: VIEW_DOCTOR,
          payload: { name, qualification, speciality, phone, city, coordinates, medicinesUid, medicinesData, uid }
        });
        dispatch({ type: CLEAR_DOCTOR_SPINNER });
      }).catch(function (err) {
        console.log(err);
      });
    }).catch(function (error) {
      console.log("Error getting documents: ", error);
    });

  }
}

export const resetMedicines = (medicinesUid, medicinesData) => {
  return {
    type: SET_MEDICINES,
    payload: { medicinesUid, medicinesData }
  }
}

const setMedicineData = async (dispatch, array) => {
  var db = firebase.firestore();
  var medicines = [];
  for (var i = 0; i < array.length; i++) {
    await db.collection('medicines').doc(array[i]).get().then(function (doc) {
      var medicine = doc.data();
      medicine.uid = doc.id;
      medicines.push(medicine);
    });
  }
  dispatch({
    type: SET_SELECTED_MEDICINE_DATA,
    payload: medicines,
  });
}