import firebase, { firestore } from 'firebase';
import 'firebase/firestore';
import { Actions } from 'react-native-router-flux';
import {
	EMPLOYEE_UPDATE,
	EMPLOYEE_CREATE,
	EMPLOYEE_EDIT,
	SET_SELECTED_USER_DOCTORS,
	SET_USER_FORM_DOCTORS,
	EMPLOYEE_CREATE_SPINNER,
	ADD_USER_FORM_DOCTOR_SPINNER,
	REMOVE_USER_FORM_DOCTOR_SPINNER,
	EMPLOYEES_LIST_SUCCESS,
	EMPLOYEES_LIST_SPINNER,
	RESET_USER_FORM_DOCTORS,
	GET_EMPLOYEE_DOCTORS,
	EMPLOYEE_DOCTORS_SPINNER_CREATE,
	EMPLOYEE_DOCTORS_SPINNER_REMOVE,
} from './types';

export const employeeUpdate = ({ prop, value }) => {
	return {
		type: EMPLOYEE_UPDATE,
		payload: { prop, value }
	};
};


export const employeeCreate = ({ name, phone, shift }) => {
	var db = firebase.firestore();
	const { currentUser } = firebase.auth();
	// return (dispatch) => {
	// 	dispatch({ type: EMPLOYEE_CREATE_SPINNER });
	// 	firebase.database().ref(`/users/${currentUser.uid}/employees`)
	// 		.push({ name, phone, shift })
	// 		.then(() => {
	// 			dispatch({ type: EMPLOYEE_CREATE })
	// 			Actions.employeeList({ type: 'reset' })
	// 		});
	// };
	return (dispatch) => {
		dispatch({ type: EMPLOYEE_CREATE_SPINNER });
		db.collection('users').add({
			name: name,
			phone: phone,
			shift: shift,
			company: currentUser.uid,
			doctors: []
		}).then(function () {
			dispatch({ type: EMPLOYEE_CREATE })
			Actions.employeeList();
		}).catch(function (err) {
			console.log(err);
		});
	};
}

export const employeesList = (user) => {
	// return (dispatch) => {
	// 	dispatch({ type: EMPLOYEES_LIST_SPINNER });
	// 	firebase.database().ref(`users/${user.user.uid}/employees`)
	// 		.on('value', snapshot => {
	// 			dispatch({ type: EMPLOYEES_LIST_SUCCESS, payload: snapshot.val() });
	// 		});
	// };
	var db = firebase.firestore();
	return async (dispatch) => {
		dispatch({ type: EMPLOYEES_LIST_SPINNER });
		var results = [];
		await db.collection("users").get().then(function (querySnapshot) {
			querySnapshot.forEach(function (doc) {
				var user = doc.data();
				user.uid = doc.id
				results.push(user);
			});
			dispatch({ type: EMPLOYEES_LIST_SUCCESS, payload: results });
		}).catch(function (error) {
			console.log("Error getting documents: ", error);
		});
	}
};


export const employeeEdit = ({ name, phone, shift, uid }) => {
	var db = firebase.firestore();
	const { currentUser } = firebase.auth();
	return (dispatch) => {
		dispatch({ type: EMPLOYEE_CREATE_SPINNER });
		db.collection('users').doc(uid).update({
			name: name,
			phone: phone,
			shift: shift,
		}).then(() => {
			dispatch({ type: EMPLOYEE_EDIT });
			Actions.employeeList({ type: 'reset' });
		}).catch((err) => {
			console.log(err);
		});
	};
	// return (dispatch) => {
	// 	dispatch({ type: EMPLOYEE_CREATE_SPINNER });
	// 	firebase.database().ref(`users/${currentUser.uid}/employees/${uid}`)
	// 		.set({ name, phone, shift })
	// 		.then(() => {
	// 			dispatch({ type: EMPLOYEE_EDIT });
	// 			Actions.employeeList({ type: 'reset' });
	// 		});
	// };
};


export const employeeRemove = ({ uid }) => {
	var db = firebase.firestore();
	const { currentUser } = firebase.auth();
	return async dispatch => {
		await db.collection('users').doc(uid).delete().then(() => {
			Actions.employeeList({ type: 'reset' })
		}).catch(function (err) {
			console.log(err);
		});
	}
};

export const employeeReset = () => {
	return (dispatch) => {
		dispatch({ type: EMPLOYEE_EDIT });
	}
};

export const setUserDoctors = (uid) => {
	var db = firebase.firestore();
	let doctors = [];
	return dispatch => {
		dispatch({ type: ADD_USER_FORM_DOCTOR_SPINNER });
		db.collection('users').doc(uid).get().then(async function (doc) {
			var user = doc.data();
			user.uid = doc.id;
			for (let i = 0; i < user.doctors.length; i++) {
				await db.collection('doctors').doc(user.doctors[i]).get().then(function (doc) {
					var doctor = doc.data();
					doctor.uid = doc.id;
					doctors.push(doctor);
				});
			}
			let doctorsUid = user.doctors;
			dispatch({
				type: SET_SELECTED_USER_DOCTORS,
				payload: { doctorsUid, doctors }
			});
			dispatch({
				type: SET_USER_FORM_DOCTORS,
				payload: { doctorsUid, doctors }
			});
			dispatch({ type: REMOVE_USER_FORM_DOCTOR_SPINNER });
		}).catch(function (err) {
			console.log(err);
		})
	}
}
export const resetUserDoctors = (doctorsUid, doctors) => {
	return {
		type: RESET_USER_FORM_DOCTORS,
		payload: { doctorsUid, doctors }
	};
}

export const employeeRemoveSpinner = () => {
	return {
		type: EMPLOYEE_REMOVE_SPINNER,
	}
}


export const getEmployeeDoctors = (uid) => {
	var db = firebase.firestore();
	let doctors = [];
	return dispatch => {
		dispatch({ type: EMPLOYEE_DOCTORS_SPINNER_CREATE })
		db.collection('users').doc(uid).get().then(async function (doc) {
			if (doc.data().doctors) {
				for (let i = 0; i < doc.data().doctors.length; i++) {
					await db.collection('doctors').doc(doc.data().doctors[i]).get().then(function (doc) {
						var doctor = doc.data();
						doctor.uid = doc.id;
						doctors.push(doctor);
					}).catch(function (err) {
						console.log(err);
					})
				}
			}
			dispatch({
				type: GET_EMPLOYEE_DOCTORS,
				payload: { doctors, uid }
			});
			dispatch({ type: EMPLOYEE_DOCTORS_SPINNER_REMOVE })
		}).catch(function (err) {
			console.log(err);
		});
	}
}




