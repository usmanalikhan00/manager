import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import AsyncStorage from '@react-native-community/async-storage';
import {
  DOCTOR_INPUT_CHANGED,
  DOCTOR_NAME_CHANGED,
  DOCTOR_SPECIALITY_CHANGED,
  DOCTOR_QUALIFICATION_CHANGED,
  DOCTOR_PHONE_CHANGED,
  DOCTOR_INITIAL_STATE,
  CHANGE_FORM_MAP_MARKER,
  DOCTOR_CREATE_SPINNER,
  DOCTOR_REMOVE_SPINNER,
  DOCTOR_LIST_SUCCESS,
  DOCTOR_LIST_SPINNER,
  VIEW_DOCTOR_SPINNER,
  VIEW_DOCTOR,
  DOCTOR_UPDATED,
  CLEAR_SELECTED_DOCTOR,
  SET_MEDICINES,
  CLEAR_DOCTOR_SPINNER,
  SET_RENDER_DOCTOR_LIST,
  REMOVE_RENDER_DOCTOR_LIST,
  SET_SELECTED_DOCTOR_DATA,
  REMOVE_SELECTED_DOCTOR_DATA,
  SET_SELECTED_USER_DOCTORS,
  ADD_USER_FORM_DOCTOR_SPINNER,
  REMOVE_USER_FORM_DOCTOR_SPINNER,
  SET_SELECTED_DOCTOR_UID,
} from './types';


export const inputChanged = ({ prop, value }) => {
  return {
    type: DOCTOR_INPUT_CHANGED,
    payload: { prop, value }
  }
}
export const nameChanged = (value) => {
  return {
    type: DOCTOR_NAME_CHANGED,
    payload: value
  }
}

export const specialityChanged = (value) => {
  return {
    type: DOCTOR_SPECIALITY_CHANGED,
    payload: value
  }
}
export const qualificationChanged = (value) => {
  return {
    type: DOCTOR_QUALIFICATION_CHANGED,
    payload: value
  }
}
export const phoneChanged = (value) => {
  return {
    type: DOCTOR_PHONE_CHANGED,
    payload: value
  }
}

export const changeMapMarker = (coordinates) => {
  var region = {
    latitude: coordinates.latitude,
    longitude: coordinates.longitude,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
  }
  return {
    type: CHANGE_FORM_MAP_MARKER,
    payload: { region, coordinates }
  }
}


export const viewDoctor = (uid) => {
  var db = firebase.firestore();
  var doctor = null;
  return (dispatch) => {
    dispatch({ type: VIEW_DOCTOR_SPINNER });
    db.collection('doctors').doc(uid).get().then(async function (doc) {
      var medicines = [];
      doctor = doc.data();
      doctor.uid = doc.id;
      for (var i = 0; i < doctor.medicines.length; i++) {
        await db.collection('medicines').doc(doctor.medicines[i]).get().then(function (doc) {
          var medicine = doc.data();
          medicine.uid = doc.id;
          medicines.push(medicine);
        });
      }
      doctor.medicinesUid = doctor.medicines;
      doctor.medicinesData = medicines;
      const { name, qualification, speciality, phone, city, coordinates, medicinesUid, medicinesData, uid } = doctor;
      dispatch({
        type: VIEW_DOCTOR,
        payload: { name, qualification, speciality, phone, city, coordinates, medicinesUid, medicinesData, uid }
      });
      dispatch({
        type: SET_MEDICINES,
        payload: { medicinesUid, medicinesData }
      });
      dispatch({
        type: CLEAR_DOCTOR_SPINNER
      });
      // Actions.doctorView();
    }).catch(function (err) {
      console.log(err);
    })
  }
}

export const viewDoctorComplete = () => {
  return {
    type: VIEW_DOCTOR_SPINNER
  };
}

export const removeSpinner = () => {
  return {
    type: DOCTOR_REMOVE_SPINNER
  };
}


export const doctorCreate = ({ name, speciality, qualification, phone, city, coordinates }) => {
  var db = firebase.firestore();
  return async (dispatch) => {
    var currentUser = JSON.parse(await AsyncStorage.getItem('LoggedUser'));
    dispatch({ type: DOCTOR_CREATE_SPINNER });
    // Add a new document in collection "doctors"
    db.collection("doctors").add({
      name: name,
      speciality: speciality,
      qualification: qualification,
      phone: phone || '',
      city: city || '',
      coordinates: coordinates,
      medicines: [],
      company: firebase.auth().currentUser.uid,
    }).then(function () {
      dispatch({ type: DOCTOR_INITIAL_STATE })
      Actions.doctorList({ type: 'reset' })
    }).catch(function (error) {
      console.error("Error writing document: ", error);
    });
  };
}

export const doctorList = () => {
  var db = firebase.firestore();
  return async (dispatch) => {
    dispatch({ type: DOCTOR_LIST_SPINNER });
    var results = [];
    await db.collection("doctors").get().then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        var doctor = doc.data();
        doctor.uid = doc.id
        results.push(doctor);
      });
      dispatch({ type: DOCTOR_LIST_SUCCESS, payload: results });
    }).catch(function (error) {
      console.log("Error getting documents: ", error);
    });
  }
}


export const doctorEdit = (name, qualification, speciality, phone, city, coordinates, medicinesUid, medicinesData, uid) => {
  var db = firebase.firestore();
  return async (dispatch) => {
    dispatch({ type: DOCTOR_CREATE_SPINNER });
    await db.collection("doctors").doc(uid).set({
      name: name,
      qualification: qualification,
      speciality: speciality,
      phone: phone,
      city: city,
      coordinates: coordinates,
      medicines: medicinesUid,
      company: firebase.auth().currentUser.uid,
    }).then(function () {
      db.collection("doctors").doc(uid).get().then(function (doc) {
        const { name, qualification, speciality, phone, city, coordinates, } = doc.data();
        dispatch({
          type: VIEW_DOCTOR,
          payload: { name, qualification, speciality, phone, city, coordinates, medicinesUid, medicinesData, uid }
        });
        dispatch({ type: DOCTOR_REMOVE_SPINNER });
      }).catch(function (err) {
        console.log(err);
      });
    }).catch(function (error) {
      console.log("Error getting documents: ", error);
    });
  }
}

export const doctorReset = () => {
  return {
    type: DOCTOR_INITIAL_STATE
  };
}


export const setUpdateFlag = () => {
  return {
    type: DOCTOR_UPDATED
  };
}


export const clearSelectedDoctor = () => {
  return {
    type: CLEAR_SELECTED_DOCTOR
  };
}

export const setRenderDoctorList = () => {
  return {
    type: SET_RENDER_DOCTOR_LIST
  };
}

export const removeRenderDoctorList = () => {
  return {
    type: REMOVE_RENDER_DOCTOR_LIST
  };
}


export const selectDoctor = (uids, uid) => {
  var db = firebase.firestore();
  // console.log("UIDS FROM INDEXOF:--", uid, uids)
  return async dispatch => {
    if (uids.indexOf(uid) < 0) {
      let doctorsUid = uids;
      doctorsUid.push(uid);
      dispatch({
        type: SET_SELECTED_DOCTOR_UID,
        payload: doctorsUid
      });
      await db.collection('doctors').doc(uid).get().then(function (doc) {
        var doctor = doc.data();
        doctor.uid = doc.id;
        dispatch({
          type: SET_SELECTED_DOCTOR_DATA,
          payload: { doctor }
        });
      }).catch(function (err) {
        console.log(err);
      })
    }
  }
}



export const removeDoctor = (doctorsUid, doctorsData, index) => {
  return dispatch => {
    let doctorsUidCopy = [];
    let doctorsDataCopy = [];

    for (let i = 0; i < doctorsUid.length; i++) {
      if (i != index) {
        doctorsUidCopy.push(doctorsUid[i]);
        doctorsDataCopy.push(doctorsData[i]);
      }
    }

    dispatch({
      type: REMOVE_SELECTED_DOCTOR_DATA,
      payload: { doctorsUid: doctorsUidCopy, doctorsData: doctorsDataCopy }
    });
  }
}

export const updateDoctors = (doctorsUid, doctors, uid) => {
  var db = firebase.firestore();
  return async dispatch => {
    dispatch({
      type: ADD_USER_FORM_DOCTOR_SPINNER,
    });
    await db.collection('users').doc(uid).update({
      doctors: doctorsUid
    }).then(function () {
      dispatch({
        type: SET_SELECTED_USER_DOCTORS,
        payload: { doctorsUid, doctors }
      })
      dispatch({
        type: REMOVE_USER_FORM_DOCTOR_SPINNER,
      });
    }).catch(function (err) {
      console.log(err);
    })
  }
}
